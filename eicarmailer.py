#!/usr/bin/env python

__author__ = 'Grant Stavely'
__version__ = '0.0.1'
__date__ = '2012/11/09'

"""
2012/11/09: 
  * lots stollen from http://stackoverflow.com/questions/1966073/how-do-i-send-attachments-using-smtp
  * other bits from charm.py
2012/11/10:
  * made the attachment a valid pdf because of my crippling need to tinker with crap that already works
  * learned that optparse is deprecated in modern python and cared not once
  * added clean message option (with it's own clean valid pdf) 
  * h/t didier stevens make-pdf-embedded.py for the pdf, as one does
  * thought about adding MX lookups, decided to have more coffee instead
"""
import warnings
import optparse
import sys
import os
import smtplib
import email
import email.encoders
import email.mime.text
import email.mime.base
import syslog 
import base64

"""
Utility functions
"""
def log(message):
    if options.syslog:
        syslog.openlog("%s[%d]" % (os.path.basename(sys.argv[0]), os.getpid()),0, getattr(syslog, options.facility))
        syslog.syslog(getattr(syslog, options.priority), message)
    print message

def verbose(message):
    if options.verbose:
        log("verbose: %s" % message)

def buildLogOption(option, opt_str, value, parser):
    log = re.compile("^LOG")
    logmatch = log.match(value)
    value = value.upper()
    if not logmatch:
        value = "LOG_" + value
    if value in dir(syslog):
        parser.values.syslog = True
        setattr(parser.values, option.dest, value)
    else:
        raise Exception("'" + value + "' is not a valid log option")

"""
Parse the command line.
"""
parser = optparse.OptionParser(usage='usage: %prog (options)', version='%prog ' + __version__)
parser.add_option('-v', '--verbose', action='store_true', default=False, help='verbose output')
parser.add_option('-r', '--recipient', action='store', default='', type="string", nargs=1, metavar='RECIPIENT', help='send Report email to RECIPIENT')
parser.add_option('-m', '--mta', action='store', default='', type="string", nargs=1, metavar='SERVER', help='use SERVER as MTA')
parser.add_option('-c', '--clean', action='store_true', default=False, help='send a clean message without EICAR attached')
parser.add_option("--syslog", "-S", action="store_true", \
    dest="syslog", default=False, help="Write to syslog")
parser.add_option("--facility", "-F", action="callback", type="string", nargs=1, \
    dest="facility", default="LOG_USER", callback=buildLogOption, help="syslog facility. Defaults: 'user')")
parser.add_option("--priority", "-p", action="callback", type="string", nargs=1, \
    dest="priority", default="LOG_INFO", callback=buildLogOption, help="syslog priority. Defaults: 'info'")
(options, args) = parser.parse_args()
if options.verbose:
    print "Program: %s" % os.path.basename(sys.argv[0])
    print "Options:",
    for option in parser.parse_args():
        print option

"""
Begin
"""
if (options.recipient == '') or (options.mta == ''): 
  parser.print_help()
  print('')
  print('Cowardly refusing to continue without at least a recipient an mta.')
  sys.exit()

"""
Address the message.
"""
smtpserver = options.mta
to = options.recipient
fromAddr = options.recipient
if options.clean:
    subject = "This is just a test message. EICAR was not attached"
else:
    subject = "EICAR antivirus test string is attached to this message"

"""
Add html
"""
html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" '
html +='"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml">'
html +='<body style="font-size:12px;font-family:Verdana"><p>This is a test message.<p>'
html += "</body></html>"
emailMsg = email.MIMEMultipart.MIMEMultipart('alternative')
emailMsg['Subject'] = subject
emailMsg['From'] = fromAddr
emailMsg['To'] = to
emailMsg.attach(email.mime.text.MIMEText(html,'html'))

"""
Add Eicar
"""
fileMsg = email.mime.base.MIMEBase('application','pdf')
if options.clean:
    fileMsg.set_payload(base64.b64decode("JVBERi0xLjEKJdDQ0NAKCjEgMCBvYmoKPDwKIC9UeXBlIC9DYXRhbG9nCiAvT3V0bGluZXMgMiAwIFIKIC9QYWdlcyAzIDAgUgogL05hbWVzIDw8IC9FbWJlZGRlZEZpbGVzIDw8IC9OYW1lcyBbKC9kZXYvbnVsbCkgNyAwIFJdID4+ID4+Cj4+CmVuZG9iagoKMiAwIG9iago8PAogL1R5cGUgL091dGxpbmVzCiAvQ291bnQgMAo+PgplbmRvYmoKCjMgMCBvYmoKPDwKIC9UeXBlIC9QYWdlcwogL0tpZHMgWzQgMCBSXQogL0NvdW50IDEKPj4KZW5kb2JqCgo0IDAgb2JqCjw8CiAvVHlwZSAvUGFnZQogL1BhcmVudCAzIDAgUgogL01lZGlhQm94IFswIDAgNjEyIDc5Ml0KIC9Db250ZW50cyA1IDAgUgogL1Jlc291cmNlcyA8PAogICAgICAgICAgICAgL1Byb2NTZXQgWy9QREYgL1RleHRdCiAgICAgICAgICAgICAvRm9udCA8PCAvRjEgNiAwIFIgPj4KICAgICAgICAgICAgPj4KPj4KZW5kb2JqCgo1IDAgb2JqCjw8IC9MZW5ndGggNDUgPj4Kc3RyZWFtCkJUIC9GMSAxMiBUZiA3MCA3MDAgVGQgMTUgVEwgKFlheSBQREZzKSBUaiBFVAplbmRzdHJlYW0KZW5kb2JqCgo2IDAgb2JqCjw8CiAvVHlwZSAvRm9udAogL1N1YnR5cGUgL1R5cGUxCiAvTmFtZSAvRjEKIC9CYXNlRm9udCAvSGVsdmV0aWNhCiAvRW5jb2RpbmcgL01hY1JvbWFuRW5jb2RpbmcKPj4KZW5kb2JqCgo3IDAgb2JqCjw8CiAvVHlwZSAvRmlsZXNwZWMKIC9GICgvZGV2L251bGwpCiAvRUYgPDwgL0YgOCAwIFIgPj4KPj4KZW5kb2JqCgo4IDAgb2JqCjw8CiAvTGVuZ3RoIDgKIC9GaWx0ZXIgL0ZsYXRlRGVjb2RlCiAvVHlwZSAvRW1iZWRkZWRGaWxlCj4+CnN0cmVhbQp4nAMAAAAAAQplbmRzdHJlYW0KZW5kb2JqCgp4cmVmCjAgOQowMDAwMDAwMDAwIDY1NTM1IGYgCjAwMDAwMDAwMTYgMDAwMDAgbiAKMDAwMDAwMDE0NyAwMDAwMCBuIAowMDAwMDAwMTk2IDAwMDAwIG4gCjAwMDAwMDAyNTcgMDAwMDAgbiAKMDAwMDAwMDQ0OSAwMDAwMCBuIAowMDAwMDAwNTQ1IDAwMDAwIG4gCjAwMDAwMDA2NTkgMDAwMDAgbiAKMDAwMDAwMDczNCAwMDAwMCBuIAp0cmFpbGVyCjw8CiAvU2l6ZSA5CiAvUm9vdCAxIDAgUgo+PgpzdGFydHhyZWYKODM2CiUlRU9GCg=="))
    fileMsg.add_header('Content-Disposition','attachment;filename=harmless-test-file.pdf')
else:
    fileMsg.set_payload(base64.b64decode("JVBERi0xLjEKJdDQ0NAKCjEgMCBvYmoKPDwKIC9UeXBlIC9DYXRhbG9nCiAvT3V0bGluZXMgMiAwIFIKIC9QYWdlcyAzIDAgUgogL05hbWVzIDw8IC9FbWJlZGRlZEZpbGVzIDw8IC9OYW1lcyBbKEVJQ0FSKSA3IDAgUl0gPj4gPj4KPj4KZW5kb2JqCgoyIDAgb2JqCjw8CiAvVHlwZSAvT3V0bGluZXMKIC9Db3VudCAwCj4+CmVuZG9iagoKMyAwIG9iago8PAogL1R5cGUgL1BhZ2VzCiAvS2lkcyBbNCAwIFJdCiAvQ291bnQgMQo+PgplbmRvYmoKCjQgMCBvYmoKPDwKIC9UeXBlIC9QYWdlCiAvUGFyZW50IDMgMCBSCiAvTWVkaWFCb3ggWzAgMCA2MTIgNzkyXQogL0NvbnRlbnRzIDUgMCBSCiAvUmVzb3VyY2VzIDw8CiAgICAgICAgICAgICAvUHJvY1NldCBbL1BERiAvVGV4dF0KICAgICAgICAgICAgIC9Gb250IDw8IC9GMSA2IDAgUiA+PgogICAgICAgICAgICA+Pgo+PgplbmRvYmoKCjUgMCBvYmoKPDwgL0xlbmd0aCA0NSA+PgpzdHJlYW0KQlQgL0YxIDEyIFRmIDcwIDcwMCBUZCAxNSBUTCAoWWF5IFBERnMgWDVPIVAlQEFQWzRcUFpYNTQoUF4pN0NDKTd9JEVJQ0FSLVNUQU5EQVJELUFOVElWSVJVUy1URVNULUZJTEUhJEgrSCopIFRqIEVUCmVuZHN0cmVhbQplbmRvYmoKCjYgMCBvYmoKPDwKIC9UeXBlIC9Gb250CiAvU3VidHlwZSAvVHlwZTEKIC9OYW1lIC9GMQogL0Jhc2VGb250IC9IZWx2ZXRpY2EKIC9FbmNvZGluZyAvTWFjUm9tYW5FbmNvZGluZwo+PgplbmRvYmoKCjcgMCBvYmoKPDwKIC9UeXBlIC9GaWxlc3BlYwogL0YgKEVJQ0FSKQogL0VGIDw8IC9GIDggMCBSID4+Cj4+CmVuZG9iagoKOCAwIG9iago8PAogL0xlbmd0aCA3NwogL0ZpbHRlciAvRmxhdGVEZWNvZGUKIC9UeXBlIC9FbWJlZGRlZEZpbGUKPj4Kc3RyZWFtCnicizD1VwxQdXAMiDaJCYiKMDXRCIjTNHd21jSvVXH1dHYM0g0OcfRzcQxy0XX0C/EM8wwKDdYNcQ0O0XXz9HFVVPHQ9tDiAgCGbRIZCmVuZHN0cmVhbQplbmRvYmoKCnhyZWYKMCA5CjAwMDAwMDAwMDAgNjU1MzUgZiAKMDAwMDAwMDAxNiAwMDAwMCBuIAowMDAwMDAwMTQzIDAwMDAwIG4gCjAwMDAwMDAxOTIgMDAwMDAgbiAKMDAwMDAwMDI1MyAwMDAwMCBuIAowMDAwMDAwNDQ1IDAwMDAwIG4gCjAwMDAwMDA1NDEgMDAwMDAgbiAKMDAwMDAwMDY1NSAwMDAwMCBuIAowMDAwMDAwNzI2IDAwMDAwIG4gCnRyYWlsZXIKPDwKIC9TaXplIDkKIC9Sb290IDEgMCBSCj4+CnN0YXJ0eHJlZgo4OTgKJSVFT0YK"))
    fileMsg.add_header('Content-Disposition','attachment;filename=eicar-test-file.pdf')
email.encoders.encode_base64(fileMsg)
emailMsg.attach(fileMsg)

"""
Fire Away
"""
server = smtplib.SMTP(smtpserver)
server.set_debuglevel(1)
try:
  server.sendmail(fromAddr,to,emailMsg.as_string())
except Exception, error:
  log("Error sending message: %s" % error)
server.quit()
