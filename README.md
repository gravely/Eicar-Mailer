Eicar-Mailer
============

Mail yourself malware to validate MTA controls because most MTAs won't let you nowadays.

Usage
=====
    eicarmailer.py (options)

    Options:
      --version             show program's version number and exit
      -h, --help            show this help message and exit
      -v, --verbose         verbose output
      -r RECIPIENT, --recipient=RECIPIENT
                            send Report email to RECIPIENT
      -m SERVER, --mta=SERVER
                            use SERVER as MTA
      -c, --clean           send a clean message without EICAR attached
      -S, --syslog          Write to syslog
      -F FACILITY, --facility=FACILITY
                            syslog facility. Defaults: 'user')
      -p PRIORITY, --priority=PRIORITY
                            syslog priority. Defaults: 'info'
